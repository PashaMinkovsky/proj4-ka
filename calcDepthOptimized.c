// CS 61C Spring 2016 Project 4  //PASHAAAAA

// include SSE intrinsics
#if defined(_MSC_VER)
#include <intrin.h>
#elif defined(__GNUC__) && (defined(__x86_64__) || defined(__i386__))
#include <x86intrin.h>
#endif

// include OpenMP
#if !defined(_MSC_VER)
#include <pthread.h>
#endif
#include <omp.h>

#include <math.h>
#include <stdbool.h>
#include <stdio.h>

#include "calcDepthOptimized.h"
#include "calcDepthNaive.h"


#define ABS(x) (((x) < 0) ? (-(x)) : (x))
#define MAX(a, b) ((a>b) ? a : b)
#define MIN(a, b) ((a<b) ? a : b)

float displacementOpt(int dx, int dy)
{
	return sqrt(dx*dx + dy*dy);
}

/* DO NOT CHANGE ANYTHING ABOVE THIS LINE. */

void calcDepthOptimized(float *depth, float *left, float *right, int imageWidth,
	int imageHeight, int featureWidth, int featureHeight, int maximumDisplacement)
{
	/* The two outer for loops iterate through each pixel */
memset(depth, 0, imageHeight*imageWidth*sizeof(float));
int unroll1 = (2 * featureWidth + 1)/8 * 8;
int unroll2 = (2 * featureWidth + 1)/4 * 4;
int unroll3 = (2 * featureWidth + 1); //move these outside of for loops

#pragma omp parallel for
 //do i collapse? //try parallel for
	
		
	//	#pragma omp for 
		for (int y = featureHeight; y < (imageHeight - featureHeight); y++)
		{
			for (int x = featureWidth; x < (imageWidth - featureWidth); x++)
			{

				float minimumSquaredDifference = -1;
				int minimumDy = 0;
				int minimumDx = 0;
				
				// int dxstart = MAX(-maximumDisplacement, featureWidth - x);
				// int dystart = MAX(-maximumDisplacement, featureHeight - y);
				// int dxend = MIN(imageWidth - featureWidth - x - 1, maximumDisplacement);
				// int dyend = MIN(imageHeight - featureHeight - y - 1, maximumDisplacement);

				


				/* Iterate through all feature boxes that fit inside the maximum displacement box. 
				   centered around the current pixel. */
				for (int dy = MAX(-maximumDisplacement, featureHeight - y); dy <= MIN(imageHeight - featureHeight - y - 1, maximumDisplacement); dy++)
				{
					for (int dx = MAX(-maximumDisplacement, featureWidth - x); dx <= MIN(imageWidth - featureWidth - x - 1, maximumDisplacement); dx++)
					{
	
						__m128 sum = _mm_setzero_ps();
						float squaredDifference = 0;

						for (int boxX = 0; boxX < unroll1; boxX += 8) //unroll by 8 
						{
							for (int boxY = -featureHeight; boxY <= featureHeight; boxY++)
							{

								__m128 leftHolder = _mm_loadu_ps(left + (y + boxY)*imageWidth + x + boxX - featureWidth);
								__m128 rightHolder = _mm_loadu_ps(right + (y + boxY + dy)*imageWidth + x + dx + boxX - featureWidth);
								__m128 subtracted = _mm_sub_ps(leftHolder, rightHolder);
								__m128 subSquared = _mm_mul_ps(subtracted, subtracted);
								sum = _mm_add_ps(subSquared, sum);

								__m128 leftHolder2 = _mm_loadu_ps(left + (y + boxY)*imageWidth + x + boxX - featureWidth + 4);
								__m128 rightholder2 = _mm_loadu_ps(right + (y + boxY + dy)*imageWidth + x + dx + boxX - featureWidth + 4);
								__m128 subtracted2 = _mm_sub_ps(leftHolder2, rightholder2);
								__m128 subSquared2 = _mm_mul_ps(subtracted2, subtracted2);
								sum = _mm_add_ps(subSquared2, sum);
							}
						}

						for (int boxX = unroll1; boxX < unroll2; boxX += 4) //unroll by 4 //get rid of this
						{
							for (int boxY = -featureHeight; boxY <= featureHeight; boxY++)
							{
								__m128 leftHolder = _mm_loadu_ps(left + (y + boxY)*imageWidth + x + boxX - featureWidth);
								__m128 rightHolder = _mm_loadu_ps(right + (y + boxY + dy)*imageWidth + x + dx + boxX - featureWidth);
								__m128 subtracted = _mm_sub_ps(leftHolder, rightHolder);
								__m128 subSquared = _mm_mul_ps(subtracted, subtracted);
								sum = _mm_add_ps(subSquared, sum);
							}
						}


						for (int boxX = unroll2; boxX < unroll3; boxX++)
						{
							for (int boxY = -featureHeight; boxY <= featureHeight; boxY++)
							{
								int leftX = x + boxX - featureWidth;
								int leftY = y + boxY; 
								int rightX = x + dx + boxX - featureWidth;
								int rightY = y + dy + boxY;

								float difference = left[leftY * imageWidth + leftX] - right[rightY * imageWidth + rightX];
								squaredDifference += difference * difference;
							}
						}

						float array[4];
						_mm_storeu_ps(array, sum);
						squaredDifference += array[0];
						squaredDifference += array[1];
						squaredDifference += array[2];
						squaredDifference += array[3];


								

						/* 
						Check if you need to update minimum square difference. 
						This is when either it has not been set yet, the current
						squared displacement is equal to the min and but the new
						displacement is less, or the current squared difference
						is less than the min square difference.
						*/
						if ((minimumSquaredDifference == -1) || ((minimumSquaredDifference == squaredDifference)
						&& (displacementNaive(dx, dy) < displacementNaive(minimumDx, minimumDy))) || (minimumSquaredDifference > squaredDifference))
						{
							minimumSquaredDifference = squaredDifference;
							minimumDx = dx;
							minimumDy = dy;
						}
					}
				}

				/* 
				Set the value in the depth map. 
				If max displacement is equal to 0, the depth value is just 0.
				*/
				if (minimumSquaredDifference != -1 && maximumDisplacement == 0) //|| minimumSquaredDifference == -1)
				{
						depth[y * imageWidth + x] = 0;
				} else {
						depth[y * imageWidth + x] = displacementNaive(minimumDx, minimumDy);
				}
			}
		}
	}
